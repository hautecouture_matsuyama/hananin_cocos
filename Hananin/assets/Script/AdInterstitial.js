
//  UNIT_ID='349900415823399_379262822887158'

var preloadedInterstitial = null;

cc.Class({
    extends: cc.Component,

    properties: {

        text: cc.Node,
        button: cc.Node,

        isPossible: cc.Boolean, //表示可能か？初期化できたかどうかで値が変わる。
    },

    // onLoad () {},

    getIsPossible() {
        return this.isPossible;  
    },

    start() {
        this.initialize();
    },

    initialize() {
        var self = this;

        //実行時(Chromeでの確認)にエラーが出ないようにする。
        if (typeof FBInstant == 'undefined') {
            return;
        }

        FBInstant.getInterstitialAdAsync(
            '349900415823399_379262822887158',
        ).then(function (interstitial) {
            self.preloadedInterstitial = interstitial;
            return self.preloadedInterstitial.loadAsync();
        }).then(function () {
            console.log('初期化成功！');
            this.isPossible = true;
        }).catch(function (err) {
            console.log('初期化失敗！');
            this.isPossible = false;

            //PC版で失敗した場合は広告ボタンは表示しない。
            if (navigator.userAgent.match(/(iPhone|iPad|iPod|Android)/i)) {
            }else {
                var canvasManager = cc.find('Canvas').getComponent('CanvasManager');
                canvasManager.remakeLayout();
            }
        });
    },

    showAd(isReward) {
        //実行時(Chromeでの確認)にエラーが出ないようにする。
        if (typeof FBInstant == 'undefined') {
            return;
        }

        var self = this;

        if (self.preloadedInterstitial == null || typeof self.preloadedInterstitial == 'undefined')
            return;

        self.preloadedInterstitial.showAsync()
            .then(function () {
                console.log('表示成功');
                if (isReward) {
                    var canvasManager = cc.find('Canvas').getComponent('CanvasManager');
                    canvasManager.remakeLayout();
                }
            })
            .catch(function (e) {
                console.log(e.message);
            });
    }

    // update (dt) {},
});
