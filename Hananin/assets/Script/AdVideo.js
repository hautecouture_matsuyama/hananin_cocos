
//var UNIT_ID = '349900415823399_374771620002945';

var preloadedRewardedVideo = null;

cc.Class({
    extends: cc.Component,

    properties: {
        // testText: cc.Label,
        text: cc.Node,
        button: cc.Node,

        isPossible: cc.Boolean, //表示可能か？初期化できたかどうかで値が変わる。
    },

    // onLoad () {},

    getIsPossible() {
        return this.isPossible;
    },

    start() {
        this.initialize();
    },

    //広告情報の設定
    initialize() {
        var self = this;

        //実行時(Chromeでの確認)にエラーが出ないようにする。
        if (typeof FBInstant == 'undefined') {
            // self.testText.string = '広告が無効';
            return;
        }

        // self.testText.string = FBInstant;
        FBInstant.getRewardedVideoAsync(
            '349900415823399_374771620002945',
        ).then(function (rewarded) {
            // self.testText.string = rewarded;
            self.preloadedRewardedVideo = rewarded;
            return self.preloadedRewardedVideo.loadAsync();
        }).then(function () {
            console.log('インタースティシャル：初期化成功');
            this.isPossible = true;
        }).catch(function (err) {
            console.log('インタースティシャル：初期化失敗')
            this.isPossible = false;
        });
    },

    //広告表示
    showAd() {
        var self = this;

        //PCかスマホかで処理を分ける。
        if (navigator.userAgent.match(/(iPhone|iPad|iPod|Android)/i)) {

            if (self.preloadedRewardedVideo == null || typeof self.preloadedRewardedVideo == 'undefined')
                return;

            self.preloadedRewardedVideo.showAsync()
                .then(function () {
                    console.log('表示成功');
                    // self.testText.string = '広告表示成功';
                    var player = cc.find('Canvas/GameManager/PlayerAxis/Player').getComponent('Player');
                    player.setMuteki(true);
                })
                .catch(function (e) {
                    console.log('エラー！');
                    // self.testText.string = '広告表示失敗' + e.message;
                });
        }
        //PCでは動画が表示できないので代わりにインタースティシャルを表示させる。無敵報酬付き。
        else {
            var inter = cc.find('Canvas/Config').getComponent('AdInterstitial');
            inter.showAd(true);
            var player = cc.find('Canvas/GameManager/PlayerAxis/Player').getComponent('Player');
            player.setMuteki(true);
        }

        var canvasManager = cc.find('Canvas').getComponent('CanvasManager');
        canvasManager.remakeLayout();
    },

    // update (dt) {},
});
