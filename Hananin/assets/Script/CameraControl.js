
//揺らす量。合計は0になるようにすること。
var x = new Array(20, 6, -12, 16, -20, -14, 10, -6, 0, 0, 0, 0, 0, 0, 0);
var y = new Array(-24, 12, -6, 20, -18, -4, 16, 4, 0, 0, 0, 0, 0, 0, 0);

var i = 0;

var Player=require('Player')

cc.Class({
    extends: cc.Component,

    properties: {
        player: Player,
        camera: cc.Camera,
        isGameOver: false,
        moveEnd: false,
    },

    // onLoad () {},

    start() {
        i = 0;
    },

    //衝突時に画面(カメラ)を揺らす
    shake() {
        //一定間隔でカメラを移動させる。
        var move = cc.moveBy(0.01, x[i], y[i]);
        this.node.runAction(move)

        i++;
        if (i < 15)
            setTimeout(() => {
                this.shake();       //次の揺れを起こす。
            }, 10);

    },

    chaseZoom(pos) {
        this.shake();
        var delay = cc.delayTime(0.18);
        var move = cc.moveTo(0.2, pos);
        this.node.runAction(cc.sequence(delay,move));

        setTimeout(() => {
            setTimeout(() => {
                this.moveEnd = true;
            }, 200);

            this.isGameOver = true;
        }, 180);
    },

    update(dt) {
        if (this.isGameOver == true && this.camera.zoomRatio < 3) {
            this.camera.zoomRatio += 2 * (dt * 5);
        }
        if (this.moveEnd) {
            var player = cc.find('Canvas/GameManager/PlayerAxis/Player').getComponent('Player');
            this.node.position = player.getWorldPosition();
        }
    },
});
