
var SoundManager = require('SoundManager');
var LevelManager = require('LevelManager');

cc.Class({
    extends: cc.Component,

    properties: {

        stageNum: 1,

        pageNum: 1,

        layout: cc.Node,
        adText: cc.Node,
        overText: cc.Node,
        videoBtn: cc.Node,
        mainTuto: cc.Node,

        tutoLabel: cc.Label,

        //タイトル全般。これが消えるとタイトルUIも全て消える。
        title: {
            default: null,
            type: cc.Node
        },
        tGameOver: {
            default: null,
            type: cc.Node
        },
        //以下、タイトル3要素。
        prevButton: {
            default: null,
            type: cc.Node
        },
        nextButton: {
            default: null,
            type: cc.Node
        },
        textLevel: {
            default: null,
            type: cc.Label
        },

        //画面上部に表示するレベル
        topLevel: {
            default: null,
            type: cc.Node
        },
        //ゲーム中に表示するレベル数。
        textLevel2: {
            default: null,
            type: cc.Label
        },

        //チュートリアル
        tutos: {
            default: [],
            type: cc.Node,
        },


        //以下、参照
        gameManager: {
            default: null,
            type: cc.Node
        },
        levelManager: {
            default: null,
            type: LevelManager
        },
        soundManager:{
            default: null,
            type: SoundManager
        },

        stage: {
            get() {
                return this.stageNum;
            }
        }
    },

    //タイトルのUIの表示切替。
    setButtonActive() {
        this.topLevel.active = true;
        this.title.active = false;
    },

    setGameOver() {
        this.tGameOver.active = true;
    },

    //現在のレベルをクリアした時に呼ばれる関数。
    clearLevel() {
        
        this.stageNum++;

        //次のレベルを開放
        this.levelManager.setOpenLevel(this.stageNum);

        //最終レベルならポップアップメッセージを表示後、ステージ選択へ戻る。
        if (this.levelManager.getIsMax()) {
            cc.director.loadScene('NinjaHero');
        }

        this.playLevel(this.stageNum);
        if (this.levelManager.judgeLastLevel(this.stageNum)) {
            this.levelManager.setLastLevel(this.stageNum);
            this.gameManager.getComponent('GameManager').setCountGameOver(0);
        }
        this.textLevel2.string = this.stageNum + '  /  100';
        this.gameManager.getComponent('GameManager').createGame(this.stageNum);
    },

    startGame() {
        if (this.levelManager.judgeLastLevel(this.stageNum)) {
            this.levelManager.setLastLevel(this.stageNum);
            this.gameManager.getComponent('GameManager').setCountGameOver(0);   //ゲームオーバー回数をリセット。
        }
        this.setButtonActive();
        this.textLevel2.string = this.stageNum + '  /  100';
        this.soundManager.playSE(6);
        this.gameManager.getComponent('GameManager').createGame(this.stageNum);
    },

    playLevel(level) {
        this.stageNum = level;
        this.levelManager.setCurrentLevel(level);   //選択中のレベル数を保存。
        this.textLevel.string = '- Difficulty -\n' + this.stageNum + ' / 100';
        
        //そのレベルの四角の回転度数を計算させる。
        cc.find('Canvas/GameManager/squareParent').getComponent('SquareParent').setRotate();
        
        //最低レベルなら前のレベルのボタンを非表示にする。
        if (this.levelManager.getIsMin()) {
            if (this.prevButton.active == true)
                this.prevButton.active = false;
        }
        else
            this.prevButton.active = true;

        //最高レベルなら次のレベルのボタンを非表示にする。
        if (this.levelManager.getIsMax()) {
            if (this.nextButton.active == true)
                this.nextButton.active = false;
        }
        else
            this.nextButton.active = true;
    },
    //前のレベル
    prevLevel() {
        if (!this.levelManager.getIsMin()) {
            var prev = this.stageNum - 1;
            this.playLevel(prev);
            this.soundManager.playSE(5);
        }
    },
    //次のレベル
    nextLevel() {
        if (!this.levelManager.getIsMax()) {
            var last = this.stageNum + 1;
            this.playLevel(last);
            this.soundManager.playSE(5);
        }
    },

    onLoad() {
        this.stageNum = this.levelManager.getCurrentLevel();
        this.playLevel(this.stageNum);
        
    },

    start() {
        
    },

    //広告を表示できるかどうかを判定。
    judgeIsPossible() {
        var adInter = cc.find('Canvas/Config').getComponent('AdInterstitial');
        var adVideo = cc.find('Canvas/Config').getComponent('AdVideo');

        //両方とも広告が取得できていない場合は広告ボタンは表示しない。
        if (!adInter.getIsPossible() && !adVideo.getIsPossible())
            this.remakeLayout();
    },

    //広告視聴後のゲームオーバー画面のレイアウトの作り直し。
    remakeLayout() {
        this.adText.active = false;
        this.videoBtn.active = false;
        this.layout.setContentSize(cc.size(700, 500));
        this.overText.setPosition(cc.p(15, 100));
    },

    setTutorial() {
        this.soundManager.playSE(5);
        if (this.mainTuto.active == true) {
            //チュートリアルを1枚目に戻す。
            this.tutos[0].active = true;
            this.tutos[1].active = false;
            this.tutos[2].active = false;
            this.tutos[3].active = true;
            this.pageNum = 1;
        }
        
        this.tutoLabel.string = '1 / 2';
        this.mainTuto.active = !this.mainTuto.active;        
    },

    changePage() {
        this.soundManager.playSE(5);
        this.tutos[0].active = !this.tutos[0].active;
        this.tutos[1].active = !this.tutos[1].active;
        this.tutos[2].active = !this.tutos[2].active;
        this.tutos[3].active = !this.tutos[3].active;

        //ページ番号切り替え。

        if (this.pageNum == 1)
            this.pageNum = 2;
        else if(this.pageNum == 2)
            this.pageNum = 1;

        
            this.tutoLabel.string = this.pageNum + ' / 2';
    }

    // update (dt) {},
});
