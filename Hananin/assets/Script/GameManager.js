

const floorPosition = 145;
const jumpHeight = 350;

var Player = require('Player');
var PlayerRot = require('PlayerRot');
var CanvasManager = require('CanvasManager');
var SoundManager = require('SoundManager');

cc.Class({
    extends: cc.Component,

    properties: {
        //四角の情報
        squares: {
            default: [],
            type: cc.Node,
        },

        squareParent: {
            default: null,
            type: cc.Node
        },

        level: 1,           //現在のレベル。

        countWhite: 0,      //白の残数
        numOnCircle: 1,     //四角の数
        rotateVector: 1,    //回転方向

        ///判定
        firstStart: true,       //初回プレイかどうか。
        isStart: true,
        success: true,
        isGameOver: false,

        ///ここから参照
        
        player: Player,
        canvasManager: CanvasManager,
        soundManager: SoundManager,
        spawnSystem: cc.Node,
        rotPlayer: PlayerRot,
        levelManager: cc.Node,

        _isGameOver: {
            get() {
                return this.isGameOver;
            },
        }
    },


    setJumpEvent() {
        //ラムダ式内でthisを使えるように「self」を作る。
        var self = this;
        //シングルタッチ。
        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouch: true,
            //タッチ開始時の処理
            onTouchBegan: function (touch, event) {
                //タッチされた時に呼ぶメソッド。
                self.doJump();
                //以降のイベントも取得する場合はtrueを返す。
                return true;
            },
            onTouchEnded: function (touch, event) {
                self.doWalk();
                return false;
            }
        }, self.node);
    },

    setCountGameOver(count) {
        cc.sys.localStorage.setItem('countGameOver', count);
    },

    getCountGameOver() {
        var data = cc.sys.localStorage.getItem('countGameOver');
        if (data != null)
            return Number(data);
        else
            return 0;
    },

    doJump() {
        if (this.isGameOver || this.isStart)
            return;

        //移動速度を一定化させるため、補正値を設定。
        var ratio = Math.abs((this.player.node.getPositionY() - jumpHeight) / (jumpHeight - floorPosition));
        //ジャンプ開始。
        // cc.log('--------------------------------------------------');

        // cc.log('▼ジャンプ開始');
        // cc.log('Playerの現在のY座標：' + this.player.node.getPositionY() + '    Playerの移動量：' + (jumpHeight - this.player.node.getPositionY()));
        this.soundManager.playSE(0);
        this.player.goJump(ratio * 0.1, 0, jumpHeight - this.player.node.getPositionY());
    },

    doWalk() {
        if (this.isGameOver || this.isStart)
            return;

        var ratio = Math.abs((this.player.node.getPositionY() - floorPosition) / (jumpHeight - floorPosition));
        // cc.log('▼降下開始');
        // cc.log('Playerの現在のY座標：' + this.player.node.getPositionY() + '    Playerの移動量：' + (-(this.player.node.getPositionY() - floorPosition)));
        this.player.goWalk(ratio * 0.1, 0, -(this.player.node.getPositionY() - floorPosition));
    },


    onLoad() {
        this.setJumpEvent();
        cc.eventManager.resumeTarget(this.node);
        this.isStart = true;
    },

    //ゲーム開始時の初期化処理。スタートボタンが押された時に呼び出される。
    createGame(_level) {
        this.isGameOver = false;
        this.success = true;

        this.level = _level;     //レベル数設定。
        this.numOnCircle = this.levelManager.getComponent('LevelManager').getNumSquare();    //四角数取得

        this.rotateVector = 1;
        if (this.level % 2 == 0)
            this.rotateVector = -1;

        this.squareParent.active = true;
        this.squareParent.rotation = 0;
        this.squareParent.scale = 1;

        this.player.node.opacity = 80;

        this.spawnSystem.getComponent('SpawnSystem').despawnAll();      //もし有効な四角があったらいけないので削除する。

        if (this.isStart)
            this.rotPlayer.runRotate();

        this.createStage();

        this.isStart = false;

        this.squareParent.active = true;
        this.squareParent.rotation = Math.random() * 360;
        this.squareParent.scale = 0;

        this.squareParent.getComponent('SquareParent').scaling(1);

        //ステージが最後まで出てきたら無敵を解除。
        setTimeout(() => {
            this.player.node.opacity = 255;
            this.success = false;
        }, 1500);
    },

    start() {

    },

    getClear() {
        return this.success;
    },

    getEnd() {
        return this.isGameOver;
    },

    gameOver() {
        if (this.success || this.isGameOver)
            return;

        // cc.director.getCollisionManager().enabled = false;

        cc.eventManager.pauseTarget(this.node)
        this.soundManager.stopBGM();

        this.isGameOver = true;

        this.setCountGameOver(this.getCountGameOver() + 1);
        // cc.log("ゲームオーバー");
        this.soundManager.playSE(2);
        this.rotPlayer.stopRotate();

        this.player.damage();
        setTimeout(() => {
            //インタースティシャル広告を表示させる。(30％)
            var rand = Math.floor(Math.random() * 10);
            if (rand <= 3) {
                var inter = cc.find('Canvas/Config').getComponent('AdInterstitial');
                inter.showAd(false);
            }
            setTimeout(() => {
                this.soundManager.playSE(4)
            }, 100);
            this.canvasManager.setGameOver();
        }, 1680);

    },

    spawnParticle(pos) {
        // if (parent.node.activeInHierarchy == true)
        this.countWhite--;

        this.spawnSystem.getComponent('SpawnSystem').spawnParticle(pos);
        this.soundManager.playSE(1);

        this.checkIfSuccess();
    },

    //クリアチェック
    checkIfSuccess() {
        // if (this.success)
        //     return;

        // cc.log('残りの白：' + this.countWhite);

        if (!this.isGameOver && this.countWhite <= 0) {
            this.success = true;
            this.setCountGameOver(0);
        }

        if (this.success && !this.isGameOver && this.countWhite <= 0) {
            // cc.log('クリアしたよ！');
            this.soundManager.playSE(3);

            this.squares = new Array();
            this.player.node.opacity = 80;

            this.spawnSystem.getComponent('SpawnSystem').clearBOX();
            //1.5秒かけて四角を小さくし、終わったら次のレベルへ。
            this.squareParent.getComponent('SquareParent').scaling(0);
            setTimeout(() => {
                this.canvasManager.clearLevel();
            }, 1500);
        }

    },

    reloadScene() {
        if (!this.isGameOver)
            return;

        this.soundManager.playSE(6);
        setTimeout(() => {
            this.soundManager.stopAll();
            cc.director.loadScene('NinjaHero');
        }, 548);

    },

    //ステージ構成の設定
    createStage() {

        var rand = this.level % 6;

        switch (rand) {
            case 0:
                this.createCircle(1, 1, 1); //円形(高さ均一)
                break;
            case 1:
                this.createCircle(1, 2, 3); //円形(高さ3段階)
                break;
            case 2:
                this.CreateSpiral();        //渦巻き型
                break;
            case 3:
                this.createTriangle();      //図形型
                break;
            case 4:
                this.createUpDown();        //上下一体型
                break;
            case 5:
                this.createLoose();         //不規則?型
                break;
        }

        this.createBlackSquare();
    },

    createBlackSquare() {

        //四角の並べ替え。
        this.squares = this.spawnSystem.getComponent('SpawnSystem').sortSquare();

        //黒の数を計算。
        var numBlackTotal = 1;

        var temp = 5 - this.level % 4;

        //ゲームオーバー回数に応じて生成個数を変更する。
        temp = Object.keys(this.squares).length / (1 + temp + Math.ceil(this.getCountGameOver() / 3));

        var iTemp = parseInt(temp);

        numBlackTotal = iTemp + 1;

        if (numBlackTotal <= 0)
            numBlackTotal = 1;

        if (this.level == 1)
            numBlackTotal = 0;

        // cc.log('黒の数：' + numBlackTotal);

        //四角数が1なら黒は設定しない。
        if (Object.keys(this.squares).length != 1) {
            this.spawnSystem.getComponent('SpawnSystem').spawnBlack(numBlackTotal);
        }

        //白の残数を設定する。
        this.countWhite = Object.keys(this.squares).length - numBlackTotal;
        // cc.log('白の数：' + this.countWhite);

        this.squares = this.spawnSystem.getComponent('SpawnSystem').getSpawnSquare()

    },

    createCircle(decal, gap, parralaxLength) {
        var rot = 0;
        for (var i = 0; i < this.numOnCircle; i++) {
            //四角の高さを調整。
            var height = 1;
            if (this.level % 5 > 3) {
                if (i % 2 == 0)
                    height = 0.8 - ((this.level % 2) / 10);
                else
                    height = 1 - ((this.level % 2) / 10);
            }

            //親の角度をずらす。
            this.squareParent.rotation = 360 / this.numOnCircle;
            if (this.squareParent.rotation >= 360)
                this.squareParent.rotation -= 360;
            //ずらした親の回転情報に合わせて生成することで円形に出来上がる。

            var posY = height * jumpHeight * parralaxLength * decal / (parralaxLength * decal + gap * i % parralaxLength);

            //ここから円形に生成するためのプログラム。
            var radian = (90 - this.squareParent.rotation * i) * (Math.PI / 180);

            //サイン･コサインから位置情報を設定。
            var newPositionX = posY * Math.cos(radian);
            var newPositionY = posY * Math.sin(radian);

            var pos = cc.p(newPositionX, newPositionY);
            if (i % 2 == 0) {
                var sq = this.spawnSystem.getComponent('SpawnSystem').spawnSquare(pos, rot - 180, this.squareParent);
                this.squares.push(sq);
            }
            else if (i % 2 == 1) {
                var sq = this.spawnSystem.getComponent('SpawnSystem').spawnSquare(pos, rot, this.squareParent);
                this.squares.push(sq);
            }

            rot += this.squareParent.rotation - 180;

        }
    },

    CreateSpiral() {
        var rot = 0;
        for (var i = 0; i < this.numOnCircle; i++) {

            this.squareParent.rotation = 360 / this.numOnCircle;

            var posY = jumpHeight * (100 - 1 * i) / 100;

            //ここから円形に生成するためのプログラム。
            var radian = (90 - this.squareParent.rotation * i) * (Math.PI / 180);

            //サイン･コサインから位置情報を設定。
            var newPositionX = posY * Math.cos(radian);
            var newPositionY = posY * Math.sin(radian);

            //角度が半分以上か以下かで位置情報を反転させる。
            if (this.squareParent.rotation > 180)
                newPositionX = -newPositionX;
            else
                newPositionY = -newPositionY;

            var pos = cc.p(newPositionX, newPositionY);

            var sq = this.spawnSystem.getComponent('SpawnSystem').spawnSquare(pos, -rot, this.squareParent);
            rot += this.squareParent.rotation;

            this.squares.push(sq);

        }
    },

    createTriangle() {
        var value = 1;
        var sign = 1;
        var rot = 0;

        for (var i = 0; i < this.numOnCircle; i++) {
            this.squareParent.rotation = 360 / this.numOnCircle;

            if (value > 3)
                sign = -1;
            else if (value < 2)
                sign = 1;

            var posY = jumpHeight * 6 / (5 + value);

            //ここから円形に生成するためのプログラム。
            var radian = (90 - this.squareParent.rotation * i) * (Math.PI / 180);

            //サイン･コサインから位置情報を設定。
            var newPositionX = posY * Math.cos(radian);
            var newPositionY = posY * Math.sin(radian);

            //角度が半分以上か以下かで位置情報を反転させる。
            if (this.squareParent.rotation > 180)
                newPositionX = -newPositionX;
            else
                newPositionY = -newPositionY;

            var pos = cc.p(newPositionX, newPositionY);

            var sq = this.spawnSystem.getComponent('SpawnSystem').spawnSquare(pos, -rot, this.squareParent);
            value += sign;
            rot += this.squareParent.rotation;

            this.squares.push(sq);
        }
    },

    createUpDown() {
        //ここでは線との被りを防ぐため、描画順を設定する。

        var rot = 0;
        var i = 0;
        while (Object.keys(this.squares).length < this.numOnCircle) {

            this.squareParent.rotation = 360 / (this.numOnCircle / 2);

            //ここから円形に生成するためのプログラム。
            var radian = (90 - this.squareParent.rotation * i) * (Math.PI / 180);

            //サイン･コサインから位置情報を設定。
            var newPositionX = jumpHeight * Math.cos(radian);
            var newPositionY = jumpHeight * Math.sin(radian);

            var pos = cc.p(newPositionX, newPositionY);

            var newRot = rot - 180;

            var sq = this.spawnSystem.getComponent('SpawnSystem').spawnSquare(pos, newRot, this.squareParent, 0);
            this.squares.push(sq);
            //下段の四角の生成。

            newPositionX = newPositionX * 0.7;
            newPositionY = newPositionY * 0.7;
            var pos2 = cc.p(newPositionX, newPositionY);

            var sq2 = this.spawnSystem.getComponent('SpawnSystem').spawnSquare(pos2, newRot, this.squareParent, 1);
            this.squares.push(sq2);

            rot += this.squareParent.rotation;

            i++;
        }
    },

    createLoose() {
        var rot = 0;

        var i = 0;


        var prevHeight = 0;
        var curHeight = 0;

        while (i < this.numOnCircle) {

            //乱数で高さを調整。ただし、前の高さと同じ高さは認めねーッ
            while (prevHeight == curHeight) {
                var ran = Math.floor(Math.random() * 4);
                curHeight = 1 - ran * 0.14;
            }

            this.squareParent.rotation = 360 / (this.numOnCircle);

            if (this.squareParent.rotation > 360)
                this.squareParent.rotation -= 360;

            var posY = jumpHeight * curHeight;

            //ここから円形に生成するためのプログラム。
            var radian = (90 - this.squareParent.rotation * i) * (Math.PI / 180);

            //サイン･コサインから位置情報を設定。
            var newPositionX = posY * Math.cos(radian);
            var newPositionY = posY * Math.sin(radian);

            var pos = cc.p(newPositionX, newPositionY);

            var newRot = rot - 180;

            var sq = this.spawnSystem.getComponent('SpawnSystem').spawnSquare(pos, newRot, this.squareParent);
            this.squares.push(sq);

            rot += this.squareParent.rotation;

            prevHeight = curHeight;
            i++;
        }
    },

    // update (dt) {},
});
