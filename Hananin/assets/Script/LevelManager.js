
const MAXLEVEL = 100;

//レベルデータの保存先
var LevelData = {
    lastLevel: 1,
    currentLevel: 1,
    openLevel: 1
};

cc.Class({
    extends: cc.Component,

    properties: {

        _levelData: null,   //保存先LevelDataへアクセスする変数
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this._levelData = LevelData;
        this.roadLevel();

        //もし上限レベルを超えていたら戻す。
        if (this._levelData.currentLevel > MAXLEVEL) {
            this._levelData.currentLevel = MAXLEVEL;
        }
        

        // cc.log('現在のレベル：' + this._levelData.currentLevel);
        // cc.log('開放レベル：' + this._levelData.openLevel);
        this.setInputControl();
    },

    start () {

    },

    roadLevel() {
        //引数に指定したタグの情報を読み込む。
        var data = cc.sys.localStorage.getItem('LevelData');

        if (data != null) {
            //データが見つかった場合
            this._levelData = JSON.parse(data);
        }
        else {
            //見つからなかった場合、初期値を代入
            this._levelData = LevelData;
            //おそらく初回プレイ時かと思われるので、チュートリアル画面の表示をする。
            var canvasManager = cc.find('Canvas').getComponent('CanvasManager');
            canvasManager.setTutorial();
        }
    },

    setInputControl(){
        var self= this;
        //キーボードのイベントリスナーを追加。
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            //キーを押した時に、それが方向キーなら対応する方向に加速度を設定。
            onKeyPressed: function(keyCode,event){
                switch (keyCode) {
                    case cc.KEY.c:
                        self.setOpenLevel(1);
                        break;
                }
            },
        },self.node)
    },

    saveLevel() {
        //JSONオブジェクトを文字列にして保存
        cc.sys.localStorage.setItem('LevelData',JSON.stringify(this._levelData))
    },

    //以下、外部用
    getCurrentLevel() {
        this.roadLevel();
        //↑の時点では文字列なので変換する必要がある
        this._levelData.currentLevel = Number(this._levelData.currentLevel);

        return this._levelData.currentLevel;
    },

    setCurrentLevel(level) {
        this._levelData.currentLevel = level;
        this.saveLevel();
    },

    getOpenLevel() {
        this.roadLevel();
        this._levelData.openLevel = Number(this._levelData.openLevel);

        return this._levelData.openLevel;
    },

    setOpenLevel(level) {
        if (level == 1)
            cc.log('リセット');
        
        //クリア時に双方のレベル数が一致かつ、レベル上限ではなかったら次のレベルを解放する
        if (this._levelData.currentLevel == this._levelData.openLevel && this._levelData.openLevel < MAXLEVEL) {
            this._levelData.openLevel = level;
            this.saveLevel();
        }
    },

    getIsMax() {
        var isMax = false;
        if (this._levelData.currentLevel >= this._levelData.openLevel) {
            isMax = true;
        }

        return isMax;
    },

    getIsMin() {
        var isMin = false;
        if (this._levelData.currentLevel <= 1) {
            isMin = true;
        }
        return isMin;
    },

    //最後にプレイしたレベルと違うレベルを選んでいるか判定。ゲームオーバー回数の更新の際に使われる。同じならtrueを返す。
    judgeLastLevel(level) {
        if (level != this._levelData.lastLevel)
            return true;
        else
            return false;
    },

    setLastLevel(level) {
        this._levelData.lastLevel = level;
        this.saveLevel();
    },

    //レベルごとの四角総数を取得。
    getNumSquare() {
        var numSQ = Math.round(10 + this._levelData.currentLevel % 35);

        //特定のレベルは数を指定する。(チュートリアルを兼ねる)

        if (this._levelData.currentLevel < 12)
            numSQ = 5;
        if (this._levelData.currentLevel == 1)
            numSQ = 1;
        if (this._levelData.currentLevel == 2)
            numSQ = 2;
        
        numSQ = Number(numSQ);
        
        return numSQ;
    },


    // update(dt) {},
});
