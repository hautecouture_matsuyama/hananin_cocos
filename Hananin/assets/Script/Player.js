
const floorPosition = 145;
const jumpHeight = 350;

cc.Class({
    extends: cc.Component,

    properties: {

        shine: cc.Node,
        //以下のBooleanでジャンプ中かどうかの判定をする。
        isJump: false,
        isWalk: true,

        //リロード時にも値を保持するため、ストレージに保存させる。
        isMuteki: false,
    },

    getIsMuteki() {
        var m = cc.sys.localStorage.getItem('boolMuteki');
        if (m == 1)
            return true;
        else
            return false;
    },

    setMuteki(value) {
        this.isMuteki = value;
        if (value == true)
            cc.sys.localStorage.setItem('boolMuteki', 1);
        else {
            var sound = cc.find('Canvas').getComponent('SoundManager');
            sound.playSE(7);
            cc.sys.localStorage.setItem('boolMuteki', 0);
            this.shine.active = false;
        }
    },

    goJump(duration, x, y) {

        this.isJump = true;
        //処理中なら中断
        if (this.isWalk) {
            this.node.stopAllActions();
            this.isWalk = false;
        }

        var Jump = cc.moveBy(duration, x, y);

        this.node.runAction(Jump);
        setTimeout(() => {
            if (this.isWalk)
                this.isJump = false;
        }, duration * 1000);
    },

    goWalk(duration, x, y) {

        this.isWalk = true;
        //処理中なら中断する。
        if (this.isJump) {
            this.node.stopAllActions();
            this.isJump = false;
        }


        var Walk = cc.moveBy(duration, x, y);

        this.node.runAction(Walk);
        setTimeout(() => {
            if (this.isJump)
                this.isWalk = false;
            this.checkIsLand();
        }, duration * 1000);
    },

    //空中で止まってないかの確認
    checkIsLand() {
        if (this.node.getPositionY() > 145) {
            // cc.log('空中停止');
            // var ratio = Math.abs((this.node.getPositionY() - floorPosition) / (jumpHeight - floorPosition));
            // this.goWalk(ratio * 0.1, 0, -(this.node.getPositionY() - floorPosition));
        }
    },

    getWorldPosition() {
        //ワールド座標にする。親の位置情報が影響されるので次の行で修正を加える。
        var worldPos = this.node.parent.convertToWorldSpace(this.node.getPosition());
        //Canvasの位置情報を減算すればワールド座標の出来上がり。
        worldPos.x -= cc.find('Canvas').getPositionX();
        worldPos.y -= cc.find('Canvas').getPositionY();

        return worldPos;
    },

    damage() {

        var worldPos = this.getWorldPosition();

        var camera = cc.find('Canvas/GameManager/Camera').getComponent('CameraControl');
        camera.chaseZoom(worldPos);

        this.node.stopAllActions();

        setTimeout(() => {
            var damage = cc.rotateBy(0.1, 170);
            //地面に落ちる。
            var fall = cc.moveBy(0.2, 0, -(this.node.getPositionY() - 145));
            this.node.runAction(cc.spawn(damage, fall));
        }, 680);

    },

    // onLoad () {},

    start() {
        this.isMuteki = this.getIsMuteki();
        if (this.isMuteki == true)
            this.shine.active = true;
    },

    // update(dt) {},
});
