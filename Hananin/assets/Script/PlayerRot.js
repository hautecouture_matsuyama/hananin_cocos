

cc.Class({
    extends: cc.Component,

    properties: {
    },

    setRotate() {

    },

    playerRotate() {
        //プレイヤーの回転移動
        var moveRight = cc.rotateBy(5, 360);
        var turn = cc.rotateBy(0.15, 0, -180);
        var moveLeft = cc.rotateBy(5, -360);
        return cc.repeatForever(cc.sequence(moveRight, turn, moveLeft, turn));
    },

    runRotate() {
        this.sequence = this.playerRotate();
        this.node.runAction(this.sequence);
    },

    stopRotate() {
        this.node.stopAllActions();
    },

    onLoad() {

    },

    start() {

    },

    // update (dt) {},
});
