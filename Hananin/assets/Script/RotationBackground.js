
const minRot = 360;
const maxRot = 520;

const minTime = 3;
const maxTime = 10;

cc.Class({
    extends: cc.Component,

    properties: {     
        isEnd: true,
    },    

    randomRot() {
        //回転方向を決める
        var rotVector = 1;
        if (Math.floor(Math.random() * 3) == 0)
            rotVector = -1;
        
        var rot = Math.random() * (maxRot + 1 - minRot) + minRot;
        return rot * rotVector;
    },

    randomDuration() {
        
        var time = Math.random() * (maxTime + 1 - minTime) + minTime;
        return time;
    },

    rotSequence() {
        var duration = this.randomDuration();

        this.isEnd = false;
        var rotStart = cc.rotateBy(duration, this.randomRot()).easing(cc.easeInOut(3));
        this.node.runAction(rotStart);

        setTimeout(() => {
            this.isEnd = true;
        }, duration*1100);
    },

    // LIFE-CYCLE CALLBACKS:

    //onLoad() {},

    start() {
    },

    update(dt) {
        if (this.isEnd)
            this.rotSequence();
    },
});
