
var Sound = cc.Class({
    name: 'Sound',
    properties: {
        url: cc.AudioClip
    },
});

cc.Class({
    extends: cc.Component,

    properties: {

        stageBGM: {
            default: null,
            type:cc.AudioSource
        },

        ////  SE一覧→    0:ジャンプ、1:花火、2:ダメージ、3:ステージクリア、4:ゲームオーバー、5:選択音、6:決定ボタン 7:無敵時接触
        sounds: {
            default: [],
            type: Sound
        },
        volumeSE: 1.5,

        isInsert: false,    //特定のSEを鳴らす際にBGMのボリュームを調節するスイッチとして使う。
        insertLock: false,  //Updateで連続で受け付けないための鍵
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
    },

    stopBGM() {
        this.stageBGM.mute = true;
    },

    stopAll() {
        cc.audioEngine.stopAll();
    },

    start () {
    },
    
    fanfare() {
        this.insertLock = true;
        cc.audioEngine.play(this.sounds[3].url, false, 2);
        setTimeout(() => {
            this.isInsert = false;
            this.insertLock = false;
        }, 1906);
    },

    playSE(sw) {
        //3番のファファーレの場合はボリュームを調整させる。
        if (sw == 3) {
            this.isInsert = true;
            return;
        }
        else if (sw == 4)
            this.volumeSE = 3;
        else if (sw == 7)
            this.volumeSE = 4;
        else
            this.volumeSE = 1.5;
        
        cc.audioEngine.play(this.sounds[sw].url, false, this.volumeSE);
    },

    //ここではisInsertをトリガーに、音量調節を行う。
    update(dt) {
        if (this.isInsert && this.stageBGM.volume > 0)
            this.stageBGM.volume -= 4 * dt;
        else if (this.isInsert && !this.insertLock && this.stageBGM.volume <= 0)
            this.fanfare();
        else if (!this.isInsert && this.stageBGM.volume < 0.7)
            this.stageBGM.volume += 1.3 * dt;
        
    },
});
