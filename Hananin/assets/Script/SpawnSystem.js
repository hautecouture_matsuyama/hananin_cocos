//四角やパーティクルの生成システム。

// var Square = require('GameManager');

cc.Class({
    extends: cc.Component,

    properties: {

        //生成するプレハブ。
        squarePrefab: {
            default: null,
            type: cc.Prefab
        },

        // FoundationPrefab: {
        //     default: null,
        //     type:cc.Prefab
        // },

        particlePrefab: {
            default: null,
            type: cc.Prefab
        },
        //格納先。
        
        squareBOX: {
            default: [],
            type: cc.Node
        },
        //こちらはスポーンした四角の格納先。GameManagerと共有する。
        squareShareBOX: {
            default: [],
            type: cc.Node
        },

        particleBOX: {
            default: [],
            type: cc.Node
        },

        // FoundationBOX: {
        //     default: [],
        //     type: cc.Node,
        // },


    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.prepareSpawn();
    },

    start() {
    },

    //前もって生成しておき、activeを変化させて表示をする。
    prepareSpawn() {
        while (Object.keys(this.squareBOX).length < 50) {
            this.squareBOX.push(this.doInstantiate(this.squarePrefab));
        }
        while (Object.keys(this.particleBOX).length < 10) {
            this.particleBOX.push(this.doInstantiate(this.particlePrefab));
        }
    },

    //全削除。
    despawnAll() {
        this.despawnAllSquares();
        this.despawnAllParticles();
    },

    despawnAllSquares() {
        var objActive = this.squareBOX.filter(o => o.activeInHierarchy == true);

        if (Object.keys(objActive).length > 0) {
            for (var i = 0; i < Object.keys(objActive).length; i++) {
                this.despawnSquare(objActive[i]);
            }
        }
    },

    despawnAllParticles() {
        var objActive = this.particleBOX.filter(o => o.activeInHierarchy == true);

        if (Object.keys(objActive).length > 0) {
            for (var i = 0; i < Object.keys(objActive).length; i++) {
                this.despawnParticle(objActive[i]);
            }
        }
    },

    //実際の生成場所。親情報、アクティブ情報もここで設定する。
    doInstantiate(obj) {
        var o = cc.instantiate(obj);
        o.parent = this.node;
        o.active = false;
        return o;
    },

    //四角生成。一つだけ生成する。
    spawnSquare(pos, rot, parent, ind) {

        if (Object.keys(this.squareBOX).length > 0) {
            //非アクティブの四角を検索し、新たに配列を作る。
            var sq = this.squareBOX.filter(o => o.activeInHierarchy == false);
            
            //非アクティブの四角が見つからなかった場合、新たに四角生成。
            if (sq == null || Object.keys(sq).length == 0) {
                cc.log('非アクティブの四角が見つかりませんでした');
                var obj = this.doInstantiate(this.squarePrefab);
                this.squareBOX.push(obj);
                return this.spawnSquare(pos, rot, parent);
            }

            //先頭の四角に情報を設定。
            sq[0].active = true;
            sq[0].parent = parent;
            sq[0].setPosition(pos);
            sq[0].rotation = rot;
            sq[0].zIndex = ind;

            ///四角ラインの生成。
            sq[0].getComponent('SquareManager').activeLine(sq[0].getPosition(), sq[0].parent);

            this.squareShareBOX.push(sq[0]);

            return sq[0];
        }

        cc.log('そもそも四角自体ないんだけど');
        //四角自体がない場合も新しく生成
        var obj = this.doInstantiate(this.squarePrefab);
        this.squareBOX.push(obj);
        return this.spawnSquare(pos, rot, parent);
    },

    sortSquare() {
        //パターン化しないように四角の入れ替えを行こなう。
        var num = Object.keys(this.squareShareBOX).length;

        while (num > 1) {
            num--;
            var k = Math.floor(Math.random() * num + 1);
                
            var w = this.squareShareBOX[k];
            this.squareShareBOX[k] = this.squareShareBOX[num];
            this.squareShareBOX[num] = w;

        }
        return this.squareShareBOX;
    },

    spawnBlack(blackCount) {
        for (var i = 0; i < blackCount; i++){
            this.squareShareBOX[i].getComponent('SquareManager').colorChange(true);
        }
    },

    despawnSquare(obj) {
        //ここでやるのは基本的に黒だけ。
        obj.parent = this.node;
        obj.active = false;
    },

    //パーティクル生成
    spawnParticle(pos) {
        if (Object.keys(this.particleBOX).length > 0) {
            //非アクティブのパーティクルを検索。
            var pa = this.particleBOX.filter(o => o.activeInHierarchy == false);
            
            if (pa == null || Object.keys(pa).length == 0) {
                var obj = this.doInstantiate(this.particlePrefab);
                this.particleBOX.push(obj);
                return this.spawnParticle(pos);
            }

            pa[0].active = true;
            // pa[0].parent = parent;
            pa[0].setPosition(pos);
            
            return pa[0];
        }

        var obj = this.doInstantiate(this.particlePrefab)
        this.particleBOX.push(obj);
        return this.spawnParticle(parent);
    },

    despawnParticle(obj) {
        obj.parent = this.node;
        obj.active = false;
    },

    //クリア時に配列を空にする。
    clearBOX() {
        this.squareShareBOX = new Array();
    },

    getAllSquare() {
        if (Object.keys(this.squareBOX).length > 0) {
            cc.log('要素数' + Object.keys(this.squareBOX).length);
            return this.squareBOX;
        }
    },

    getSpawnSquare() {
        if (Object.keys(this.squareShareBOX).length > 0)
            return this.squareShareBOX;
    }

    // update (dt) {},
});
