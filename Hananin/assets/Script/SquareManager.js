//四角本体の処理

cc.Class({
    extends: cc.Component,

    properties: {

        //個オブジェクトの情報。
        lineBlack: {
            default: null,
            type: cc.Node
        },
        lineWhite: {
            default: null,
            type: cc.Node
        },

        spriteBlack: {
            default: null,
            type: cc.Node
        },
        spriteWhite: {
            default: null,
            type: cc.Node
        },

        isBlack: false,
        isEnter: false,    //プレイヤーと当たったかどうか。
        
        position: 0.0,

    },

    // LIFE-CYCLE CALLBACKS:

    reset() {
        this.node.rotation = 0;
        this.node.setScale(cc.Vec2.ONE);
    },

    onLoad() {
        this.isBlack = false;
        this.reset();
    },

    onEnable() {
        this.isEnter = false;
        cc.director.getCollisionManager().enabled = true;
    },

    onDisable() {
        this.colorChange(false);
    },

    activeLine(target, sParent) {
        //リスト内の位置情報に更新する。
        this.node.setPosition(target);
        this.node.parent = sParent;

        this.node.setScale(cc.Vec2.ONE);
        
        //四角とその親との距離を求める。
        this.position = cc.pDistance(target, this.node.parent.getPosition());

        // this.node.rotation = this.position;
        

        //上で求めた距離によって四角ラインの長さを設定。Xをいじると伸びて線になる。
        if (this.lineBlack != null) {
            this.lineBlack.setScale(this.position *0.5, this.lineBlack.scaleY);
            this.lineWhite.setScale(this.position *0.5, this.lineWhite.scaleY);
        }
    },

    colorChange(black) {
        this.isBlack = black;
        //黒
        this.lineBlack.active = black;
        this.spriteBlack.active = black;
        //白
        this.lineWhite.active = !black;
        this.spriteWhite.active = !black;
    },

    GameOverJudge(col)
    {
        var gameManager = cc.find('Canvas/GameManager').getComponent('GameManager');
        var player = cc.find('Canvas/GameManager/PlayerAxis/Player').getComponent('Player');
        
        // 私って本当に四角ですか？
        if (cc.rectContainsRect(this.name, 'Square'))
        {
            //プレイヤーのタグかどうか。1がプレイヤー。
            if (col.tag == 1)
            {
                if (this.isBlack) {
                    if (gameManager.getClear() == true)
                        return;
                    //無敵状態なら状態解除する。
                    if (player.getIsMuteki() == true) {
                        player.setMuteki(false);
                        return;
                    }

                    gameManager.gameOver();
                }
                else {
                    if (gameManager.getEnd() || gameManager.getClear())
                        return;
                    this.isEnter = true;

                    //四角のローカル座標をワールド座標にする。親の位置情報が影響されるので次の行で修正を加える。
                    var worldPos = this.node.parent.convertToWorldSpace(this.node.getPosition());
                    //Canvasの位置情報を減算すればワールド座標の出来上がり。
                    worldPos.x -= cc.find('Canvas').getPositionX();
                    worldPos.y -= cc.find('Canvas').getPositionY();
                    
                    //この四角のワールド座標をパーティクルの生成位置として渡してやる。
                    gameManager.spawnParticle(worldPos);
                    this.node.active = false;
                }
                
            }
        }
    },

    getIsEnter() {
        return this.isEnter;
    },

    start () {
        this.isEnable = false;
    },

    onCollisionEnter(col, self) {
        this.GameOverJudge(col);
    },

    // update (dt) {},
});
