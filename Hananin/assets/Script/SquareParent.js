
const scaleDuration = 1.5;

var LevelManager=require('LevelManager')

cc.Class({
    extends: cc.Component,

    properties: {
        levelManager: {
            default: null,
            type: LevelManager
        },

        rot: 0,

        isEnd: false,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.isEnd = false;
    },

    scaling(ratio) {
        this.node.stopAllActions();
        var scaling = cc.scaleTo(scaleDuration, ratio).easing(cc.easeInOut(3));
        this.node.runAction(scaling);

        if (ratio == 1) {
            setTimeout(() => {
                this.stageRotate()
            }, scaleDuration * 1700);
        }
    },

    stageRotate() {

        
        this.isEnd = false;

        var level = this.levelManager.getCurrentLevel();
        var delay = Number;         //次の回転をするまでの時間。スピード調整もここで。
        var accel = Number;         //加速度

        delay = ((level % 3 + 1) * 100 + 500);
        //複雑なステージ構成の場合は速度をゆっくりにする。
        if (level % 6 == 3 || level % 6 == 5)
            delay += 300;
        
        delay /= (level * 1.05);
            
        accel = 0.001;

        //レベルによって動きを切り替え。
        switch (level % 3) {
            case 0:
                var rotate = cc.rotateBy(delay, this.rot).easing(cc.easeBackInOut(accel));
                this.node.runAction(rotate);
                break;
            case 1:
                var rotate = cc.rotateBy(delay, this.rot).easing(cc.easeBackIn(accel));
                this.node.runAction(rotate);
                break;
            case 2:
                var rotate = cc.rotateBy(delay, this.rot).easing(cc.easeBackOut(accel));
                this.node.runAction(rotate);
                break;
        }
        
        // var rotate = cc.rotateBy(delay, this.rot).easing(cc.easeBackInOut(accel));
        // this.node.runAction(rotate);

        setTimeout(() => {
            this.isEnd = true;
        }, delay*1000);
    },

    start () {

    },

    setRotate() {
        this.rot = Math.random() * (1000 + 1 - 600) + 360;
    },

    update(dt) {
        if (this.isEnd)
            this.stageRotate();
    },
});
